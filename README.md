# shoponline

1. How to start
```
git clone git@gitlab.com:likang.shen.eva/shoponline.git
```

2. If there are some missing library error, please use git to install
```
python manage.py migrate
```


3. Enter our Mysql environment
```
mysql -u root -p 

create database guest character set utf8

```
And we also need to change the information in DATABASES in the file "settings.py"

4. Retry 2 and start playing!
```
python manage.py createsuperuser

python manage.py runserver
```
